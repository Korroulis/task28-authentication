/////  Required packs 

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const bcrypt = require('bcrypt');
const session = require('express-session');
//const { PORT = 3006 } = process.env;
const PORT = process.env.PORT || 3000;

require('dotenv').config();

// Initialize the App.
const app = express();

// Setup Middleware applications.
app.use(express.json());
app.use(cookieParser());
// Expose JavaScript, Images and CSS files under the alias public/
app.use('/public', express.static(path.join(__dirname, 'www')));

const { Sequelize, QueryTypes } = require('sequelize');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

/////  Add a 3rd user

app.post('/api/register', async(req, res)=>{
    const username = 'mihalis';
    const password = process.env.PASSWORD;
    try{
        const hashPassword = await bcrypt.hash(password, 10);
        const result = await sequelize.query('INSERT INTO user (username, password) VALUE (:username, :password)', {
            type: QueryTypes.INSERT,
            replacements: {
                username: username,
                password: hashPassword
            }
        })
        console.log(result);
    } catch (e) {
        return res.json(e);
    }
    return res.json('User added');
});

/////  Initialize the database connection

const sequelize = new Sequelize({
    host: process.env.DB_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    dialect: process.env.DB_DIALECT
});

///// Old code not to be used

/* app.use(session({
    secret:process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure:false,
        httpOnly: true,
        maxAge: 60000
    }
})); 
 */

// Check if we can establish a connection to the database.
sequelize.authenticate().then(d => {
    console.log('Connection established successfully');

    /////  Middleware part

    app.use(async (req, res, next) => { //  NOTE: This is an async function -> You can use await.
        //const users = await sequelize.query('SELECT * FROM user', { type: QueryTypes.SELECT });
        //console.log(users);

        // 1. Define public paths, allow / to pass through
        
        const publicPaths = ['/','/api/login','/api/register'];

        if (publicPaths.includes(req.path)) {
            return next();
        }
        
        // 2. Check if the req cookie -> token exists.

        if (!req.cookies.token) {
            return res.redirect('/');
        }

        // 3. get the token from the request cookie 
        
        const {token} = req.cookies;

        // 4. Check the database for the token and if it is active = 1

        try {
            const dbToken = await sequelize.query('SELECT * FROM usersession WHERE token = :token AND active =1', { 
                type: QueryTypes.SELECT,
                replacements: {
                    token: token
                }
            });
            const User = await sequelize.query('SELECT * FROM user WHERE username = :username', {
                type: QueryTypes.SELECT,
                replacements: {
                    username: username
                }
            });
            // console.log(dbToken);

            if (dbToken.length === 0) {
                return res.redirect(403, '/');
            }

            // 5. Check if the user linked to the token exists
            // 6. Return a status 401 If it is expired or the user does not Exists
            if ( User[0].userId !== dbToken.userId) {
                return res.redirect(401, '/');
            }

        }
        catch (e) {

        }
        // Should be the last line of code here.
        return next();
    });

    /////  Get request to the login page

    app.get('/', (req, res) => { // this is our index page (or login)
        return res.status(200).sendFile(path.join(__dirname, 'www', 'index.html'));
    });

    /////  Get to the dashboard

     app.get('/dashboard', (req, res) => { // Must be a protected page. Can't access without token cookie.
        return res.status(200).sendFile(path.join(__dirname, 'www', 'dashboard.html'));
    });  
        //return res.status(200).sendFile(path.join(__dirname, 'www', 'dashboard.html'));
    

    /////  Authentication request

    app.post('/api/login', async (req, res) => { 

        // Check the login
        // 1. Get the username and password from the req.body
        const username=req.body.username;
        const password=req.body.password;
        
        // 2. Check if the username exists (Select the user based on the username)

        if (!username || !password) {
            return res.status(400).json({
                message:'Please insert username and password'
            })
        }

        try {

            const User = await sequelize.query('SELECT * FROM user WHERE username = :username', {
                type: QueryTypes.SELECT,
                replacements: {
                    username: username
                }
            });

            if (User && User.length == 0) {
                return res.status(401).json({
                    message:'User doesnt exist'
                });
            }
            // 3. Check if the password in the db matches the password given.

            const validPass = await bcrypt.compare(password, User[0].password);

            if (!validPass) {
                return res.status(401).json({
                    message:'Invalid password'
                });
            }

             // 4. If true Generate a token using the HASH_SECRET from the .env file (it will be on the process.env)
             
            const token = await bcrypt.hash(process.env.HASH_SECRET, 10);

            // Store userId, session and token in DB

            const tokenResult = await sequelize.query('INSERT INTO usersession ( userId, token ) VALUE (:userId, :token)' , {
                type: QueryTypes.INSERT,
                replacements: {
                    userId: User[0].userId,
                    token: token
                }
            });

            // 5. NB - Send the token back to the client.
            return res.status(201).json({
                status:201,
                token: token
            });
        }
        catch (e) {
            return res.status(500).json({
                message: e.toString()
            });
        }

        /////  Old code (tried with $ but as you said it's betetr to access the dB directly as this is prone to code injection)

        // 2. Check if the username exists (Select the user based on the username)
        
        /* try {
            const users = await sequelize.query('SELECT * FROM user', { type: QueryTypes.SELECT });
            const userRecord = await users.find(obj => obj.username == `${username}`);;
            if (!userRecord) {
                throw new Error('User not found')
                // 3. Check if the password in the db matches the password given.
            } else {
                const result = await bcrypt.compare(password, users.find(o => o.username === `${username}`).password);
                if (!result) {
                    throw new Error('Incorrect password')
                  }
                }

             // 4. If true Generate a token using the HASH_SECRET from the .env file (it will be on the process.env)
            // NOTE: use bcrypt (Already required at the top) - To hash the password with 10 rounds.
            // For help see: https://www.npmjs.com/package/bcrypt 
            // Check the section on Promises:

            // Load hash from your password DB. Sample using Async/Await: 

            const token = await bcrypt.hash(process.env.HASH_SECRET, 10);

            // Store userId, session and token in DB

            const userId = users.find(o => o.username === `${username}`).userId;
            const result = await sequelize.query('INSERT INTO usersession (userId,token) VALUE (:userId, :token)', {
                type: QueryTypes.INSERT
                /* replacements: {
                    userId: username,
                    password: hashPassword
                } 
            })
            console.log([result,userId]);

            // 5. NB - Send the token back to the client. 

        }
        catch (e) {

        }*/

        // 6. End.  
          
    });

}).catch(e => {
    console.error(e);
}) 


app.listen(PORT, () => console.log(`Server has started on port ${PORT}...`));

